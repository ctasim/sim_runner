#!/usr/bin/env python3
"""
Run R0 to DL1 conversion job with N input files
"""
import argparse
import logging
import os
import sys

from pathlib import Path

from lstchain.reco import r0_to_dl1
from lstchain.paths import r0_to_dl1_filename
from lstchain.io.config import read_configuration_file


def get_parser():
    """
    Create command line options parser

    :return: Parser object
    :rtype: class `argparse.ArgumentParser`
    """
    parser = argparse.ArgumentParser()
    required_named = parser.add_argument_group('required named arguments')
    required_named.add_argument('-i', '--input-dir', help='R0 input directory path', type=Path,
                                required=True)
    required_named.add_argument('-o', '--output-dir', help='DL1 output directory path', type=Path,
                                required=True)
    required_named.add_argument('-c', '--config', help='LSTchain JSON configuration file name',
                                type=Path,
                                required=True)
    required_named.add_argument('-a', '--array-job-id', help='Batch array job ID', type=int,
                                required=True)
    parser.add_argument('-n', '--files-per-job', help='Number of files per job', type=int, default=1)
    parser.add_argument('-l', '--log-level', help='Log level', type=str, default='INFO',
                        choices=['CRITICAL', 'ERROR', 'WARNING', 'INFO', 'DEBUG'])
    return parser


class LessThanFilter(logging.Filter):
    def __init__(self, exclusive_maximum, name=""):
        super(LessThanFilter, self).__init__(name)
        self.max_level = exclusive_maximum

    def filter(self, record):
        #non-zero return means we log this message
        return 1 if record.levelno < self.max_level else 0


def main():
    """
    Parse command line input arguments and launch the sim_runner.
    """
    parser = get_parser()
    args = parser.parse_args()

    #Get the root logger
    root_logger = logging.getLogger()
    #Have to set the root logger level, it defaults to logging.WARNING
    root_logger.setLevel(logging.NOTSET)
    #Set log message format
    formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')

    logging_handler_out = logging.StreamHandler(sys.stdout)
    logging_handler_out.setLevel(level=args.log_level)
    logging_handler_out.setFormatter(formatter)
    logging_handler_out.addFilter(LessThanFilter(logging.WARNING))
    root_logger.addHandler(logging_handler_out)

    logging_handler_err = logging.StreamHandler(sys.stderr)
    logging_handler_err.setLevel(logging.WARNING)
    logging_handler_err.setFormatter(formatter)
    root_logger.addHandler(logging_handler_err)

    logger = logging.getLogger(__name__)

    input_files = [f'{args.input_dir}/{filename}' for filename in os.listdir(args.input_dir)]
    input_files_sliced = input_files[args.array_job_id * args.files_per_job:
                                     (args.array_job_id + 1) * args.files_per_job]

    output_dir = args.output_dir.absolute()
    output_dir.mkdir(exist_ok=True)

    try:
        config = read_configuration_file(args.config.absolute())
    except Exception as e:
        logger.error('Config file {args.config} could not be read:\n%s', e)
        sys.exit(1)

    for i_file in input_files_sliced:
        r0_to_dl1_fname = r0_to_dl1_filename(i_file.rsplit('/')[-1])
        output_file = output_dir / r0_to_dl1_fname
        logger.info('\nProcessing input file: %s\nOutput file: %s', i_file, output_file)
        r0_to_dl1.r0_to_dl1(i_file,
                            output_filename=output_file,
                            custom_config=config,
                            )


if __name__ == "__main__":
    main()
