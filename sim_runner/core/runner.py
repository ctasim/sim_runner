"""

IACT simulation chain runner
============================

This module provide all neccessary tools to:
    - parse the input TOML configuration
    - provide its partial validation
    - create the simulation jobs
    - launch the simulation jobs

Supported job submission engines:
    - local submission
    - PBS/SGE
    - SLURM

"""
try:
    import importlib.resources as pkg_resources
except ImportError:
    # Try backported to PY<37 `importlib_resources`.
    import importlib_resources as pkg_resources
import getpass
import json
import logging
import os
import shlex
import subprocess
import sys
import time
import traceback

from pathlib import Path
from random import randint

import toml

from sim_runner import templates

__version__ = '0.1'
__author__ = u'Mykhailo Dalchenko'

# **********************GLOBAL VARIABLES*********************

LOGGER = logging.getLogger('sim_runner')

# ***********************************************************

# *************************CONSTANTS*************************

PRODUCTION_STAGES = ('corsika', 'sim_telarray', 'dl1', 'dl2')
PRODUCTION_TYPES = ('electron', 'gamma', 'gamma_diffuse', 'pedestal', 'proton',
                    'parameter_scan', 'light_emission', 'parameter_card')
SUBMISSION_TOOLS = ('local', 'sge', 'slurm', 'htcondor')

LOGGING_LEVELS = {0: logging.CRITICAL, 1: logging.ERROR, 2: logging.WARNING,
                  3: logging.INFO, 4: logging.DEBUG}

# ***********************************************************


def setup_logging(verbosity=1):
    """
    Setup logger console and file descriptors

    Two log stream handlers are added, one for file-based logging and one for console output.
    Logging level to file is always set to DEBUG and console verbosity can be controlled.
    Verbosity levels {0,1,2} correspond to {ERROR, INFO, DEBUG}.

    :param int verbosity: Verbosity level used for console output
    """
    # set up logging to file with DEBUG level
    logging.basicConfig(level=logging.DEBUG,
                        format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                        datefmt='%m-%d %H:%M',
                        filename=f'/tmp/sim_runner_{getpass.getuser()}_{time.time()}.log',
                        filemode='w')
    # define a Handler which writes INFO messages or higher to the sys.stderr
    console = logging.StreamHandler()
    console.setLevel(LOGGING_LEVELS[verbosity])
    formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    # logging.getLogger('').addHandler(console)
    LOGGER.addHandler(console)


def find_parameter_value(input_file_name, first, last='_'):
    '''
    function that allows simtelarray to grep the value of the parameter
    used to create the corsika file, from the filename
    :param str input_file_name: the corsika input file name
    :param str first: the name of the parameter + character '_'
    :param str last: the character following the parameter value, which is '_' with the current naming.
    '''
    try:
        start = input_file_name.index(first) + len(first)
        end = input_file_name.index(last, start)
        return input_file_name[start:end]
    except ValueError:
        # TODO issue warning message or throw an exception
        return ""


def validate_config(config):
    """
    Partial `sim_runner` configuration validation.
    Direct sim_telarray/corsika options aren't checked

    Validated blocks:

    * `environment`
    * `process`

    In case configuration fails, clean exit is assured and garbage would be collected.

    :param dict config: Dictionary with the sim_runner configuration
    """
    # Validate "process" options
    if config['process']['stage'] not in PRODUCTION_STAGES:
        LOGGER.error("Production stage '%s' is not listed among allowed production types",
                     config['process']['stage'])
        clean(config)
        sys.exit(os.EX_CONFIG)

    if config['process']['production_type'] not in PRODUCTION_TYPES:
        LOGGER.error("Production type '%s' is not listed among allowed production types",
                     config['process']['production_type'])
        clean(config)
        sys.exit(os.EX_CONFIG)

    # Validate "environment" options
    if config['environment']['submission']['tool'] not in SUBMISSION_TOOLS:
        LOGGER.error("Submission tool '%s' is not listed among submission tools",
                     config['environment']['submission']['tool'])
        clean(config)
        sys.exit(os.EX_CONFIG)

    # Validate "process.scan" options
    if 'parameter_scan' in config['process']['production_type']:
        LOGGER.info("Production type is %s", config['process']['production_type'])
        try:
            trigger_scan_parameters = (config['process']['parameter_scan']['initial'],
                                       config['process']['parameter_scan']['final'],
                                       config['process']['parameter_scan']['step'])
        except KeyError as err:
            LOGGER.error("Parameteric scan field %s is missing", err)
            clean(config)
            sys.exit(os.EX_CONFIG)
        if trigger_scan_parameters[1] <= trigger_scan_parameters[0]:
            LOGGER.error("Final trigger threshold can't be less or equal to initial one")
            clean(config)
            sys.exit(os.EX_CONFIG)
    LOGGER.info("Configuration successfully validated")


def get_configuration_by_tag(config):
    """
    Download the simulation config from gitlab based on provided tag

    The method retrieves GitLab repository path and tag from the `sim_runner` configuration
    and downloads the tagged configuration to the local temporary configuration path provided
    as well in `sim_runner` configuration.

    :param dict config: Dictionary with the sim_runner configuration
    """
    stage = config['process']['stage']
    LOGGER.debug('TEST branch %s', config['environment'][stage]['tag'])
    cmd = f"""git clone --quiet -b '{config['environment'][stage]['tag']}' \
            --single-branch --depth 1 \
            {config['environment'][stage]['configuration_repository']} \
            {config['environment'][stage]['temporary_local_configuration_path']}"""
    if os.path.exists(config['environment'][stage]['temporary_local_configuration_path']):
        LOGGER.warning('Temporary local configuration path is not empty and will be overwritten!')
        subprocess.run(['rm', '-rf',
                        f"{config['environment'][stage]['temporary_local_configuration_path']}"],
                       check=True)
    try:
        subprocess.run(cmd, shell=True, check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        LOGGER.info("Downloaded sim_telarray configuration with tag %s to temporary location %s",
                    config['environment'][stage]['tag'],
                    config['environment'][stage]['temporary_local_configuration_path'])
    except subprocess.CalledProcessError as err:
        LOGGER.error("%s\nFailed to download sim-telarray configuration. "
                     "Check your web connection and main configuration file.", err)
        clean(config)
        sys.exit(os.EX_CONFIG)


def clean(config):
    """
    Clean temporary files

    :param dict config: Dictionary with the sim_runner configuration
    """
    stage = config['process']['stage']
    local_conf_path = config['environment'][stage]['temporary_local_configuration_path']
    subprocess.run(["rm", "-rf",
                    local_conf_path],
                   check=True)
    LOGGER.info("Post-running cleaning performed. Temporary files and folders have been deleted.")
    # Should we clean here the previous *dbase files from corsika, if any?


def job_command(config):
    """
    Construct single job executable command

    Currently supports only `sim_telarray`

    .. warning ::
        In the original script by David Miranda a number of paths related to sim_telarray executables
        have been added (prepending) to system $PATH variable. This is unsafe because the subsequent
        running with different versions of sim_telarray might be obscured. Let's rather try to
        call the corresponding executables

    :param dict config: Dictionary with the sim_runner configuration
    :return: A string containing the job execution command
    """
    # TODO Check installation paths
    stage = config['process']['stage']
    if stage == 'sim_telarray':
        cmd = f"{config['environment']['sim_telarray']['software_intallation_path']}/{stage} "
        cmd += (
            f"-I{config['environment'][stage]['temporary_local_configuration_path']} "
            f"-c {config['environment'][stage]['temporary_local_configuration_path']}/"
            f"{config['environment']['sim_telarray']['entry_point']} "
        )
        # Add preprocessor directives if any
        try:
            for key, value in config['sim_telarray']['preprocessor'].items():
                cmd += f"-D{key}={value} "
        except KeyError:
            pass
        for parameter_block, block_dict in config['sim_telarray'].items():
            if parameter_block == 'preprocessor':  # skip preprocessor block
                continue
            LOGGER.debug("Applying parameters from block %s", parameter_block)
            for key, value in block_dict.items():
                cmd += f"-C {key}={value} "
        LOGGER.debug("sim_telarray command:\n%s", cmd)
    elif stage == 'corsika':
        cmd = f"{config['environment']['corsika']['software_intallation_path']}/{stage} "
    elif stage == 'dl1':
        cmd = 'lstchain_mc_r0_to_dl1'
    else:
        pass
    return cmd


def create_commands(config):
    """
    Prepare simulation chain commands with provided configuration

    :param dict config: Dictionary with the sim_runner configuration
    :return: List of strings with job commands
    """
    stage = config['process']['stage']
    # Do we need to log the config here? Put just a link for the moment
    LOGGER.info("Link to the %s config: %s/-/tags/%s",
                stage,
                config['environment'][stage]['configuration_repository'],
                config['environment'][stage]['tag'])
    job_commands = []
    if stage == 'corsika':
        template_config_filename = config['environment']['corsika']['temporary_local_configuration_path'] +\
                                   '/' + config['environment']['corsika']['entry_point']
        os.makedirs(config['environment']['submission']['run_directory'], exist_ok=True)
        output_path = config['environment'][stage]['output_path']
        template_config = Path(template_config_filename).read_text()
        new_config = ''
        for run_number in range(1, int(config['corsika']['number_of_runs']) + 1):
            LOGGER.debug('run number %s', run_number)
            scan_parameters_corsika = []
            if 'parameter_scan' in config['process']['production_type']:
                scan_parameters_corsika = range(config['process']['parameter_scan']['initial'],
                                                config['process']['parameter_scan']['final'] + 1,  # include final value
                                                config['process']['parameter_scan']['step']
                                                )
                LOGGER.debug('Range of scanned parameters in corsika %s', scan_parameters_corsika)
                for scan_parameter in scan_parameters_corsika:
                    _parameter_name = config['process']['parameter_scan']['parameter']
                    LOGGER.debug('scanned parameter %s', _parameter_name)
                    LOGGER.debug('value = %s', scan_parameter)
                    LOGGER.debug('run number %s', run_number)
                    os.makedirs(f"{config['environment'][stage]['output_path']}/{_parameter_name}_{scan_parameter}",
                                exist_ok=True)
                    # Update the parameter of interest on the fly
                    if config['process'].get('storage', None) == 'temporary':
                        temp_dir = f"/tmp/{_parameter_name}_{scan_parameter}_run{run_number}"
                        os.makedirs(temp_dir, exist_ok=True)
                        temp_output_dir = f'{temp_dir}/output'
                        os.makedirs(temp_output_dir, exist_ok=True)
                        temp_output_file = f'{temp_output_dir}/{_parameter_name}_{scan_parameter}_run{run_number}'
                        output_file = temp_output_file
                        temp_log_dir = f'{temp_dir}/log'
                        os.makedirs(temp_log_dir, exist_ok=True)
                        temp_log_file = f'{temp_log_dir}/{_parameter_name}_{scan_parameter}_run{run_number}.log'
                        temp_config_dir = f'{temp_dir}/config'
                        os.makedirs(temp_config_dir, exist_ok=True)
                        temp_config_file = config['process']['production_type'] + f"_{_parameter_name}_{scan_parameter}_run{run_number}_config.cfg"
                        temp_run_dir = f'{temp_dir}/run_directory'
                        os.makedirs(temp_run_dir, exist_ok=True)
                    else:
                        output_file = f'{output_path}/{_parameter_name}_{scan_parameter}_run{run_number}'
                        log_file = f'{output_path}/{ _parameter_name}_{scan_parameter}_run{run_number}.log'
                        job_config_filename = f"{output_path}/{config['process']['production_type']}" + f"_{_parameter_name}_{scan_parameter}_run{run_number}_config.cfg"
                    LOGGER.debug("job_config_filename %s", job_config_filename)
                    new_config = template_config
                    new_config = new_config.replace('@run@', str(run_number) + str(scan_parameter))
                    new_config = new_config.replace('@output_filename@', output_file)
                    new_config = new_config.replace('@'+_parameter_name+'@', str(scan_parameter))
                    for _ in range(1, 5):
                        seed = randint(100000, 1000000)
                        new_config = new_config.replace(f'@seed{_}@', str(seed))
                    new_config_file = open(job_config_filename, 'w+')
                    new_config_file.write(new_config)
                    new_config_file.close()
                    LOGGER.debug('NEW CONFIG FILE %s', new_config_file)

                    #if 'temporary' in config['process']['storage']:
                    if config['process'].get('storage', None) == 'temporary':                    
                        job_commands.append(f'mkdir {temp_dir}; mkdir {temp_output_dir}; mkdir {temp_log_dir}; mkdir {temp_config_dir};'
                                            + f'cp {job_config_filename} {temp_config_dir}; srun '
                                            + job_command(config) + f'-o {temp_output_file} < {temp_config_dir}/{temp_config_file} > {temp_log_file};'
                                            + f'cp {temp_output_file}.corsika.gz {output_dir} ; cp {temp_log_file} {log_dir} ;'
                                            + f'rm -rf {temp_dir}'
                        )
                    else:
                        job_commands.append(job_command(config) + f'-o {output_file} < {job_config_filename} > {log_file}')

            elif 'parameter_card' in config['process']['production_type']:
                LOGGER.debug('PRODUCTION TYPE: %s', type(config['process']['parameter_card']))
                help_counter = 0
                for vals in config['process']['parameter_card']['parameter_values']:
                    new_config = template_config
                    name_prefix = "corsika_"
                    help_counter += 1
                    for par in config['process']['parameter_card']['parameter_list']:
                        LOGGER.debug("parameter %s", par)
                        val = vals[config['process']['parameter_card']['parameter_list'].index(par)]
                        LOGGER.debug("is assigned the value %s", val)
                        new_config = new_config.replace('@'+par+'@', str(val))
                        if par == 'theta' or par == 'az':
                            name_prefix += f'{par}_{val}_'

                    output_dir = f"{output_path}/node_{name_prefix}/output/"
                    os.makedirs(output_dir, exist_ok=True)
                    log_dir = f"{output_path}/node_{name_prefix}/log/"
                    os.makedirs(log_dir, exist_ok=True)
                    config_dir = f"{output_path}/node_{name_prefix}/config/"
                    os.makedirs(config_dir, exist_ok=True)
                    output_file = f'{output_dir}/{name_prefix}run{run_number}'
                    log_file = f'{log_dir}/{name_prefix}run{run_number}.log'
                    LOGGER.debug("output file name %s", output_file)
                    job_config_filename = config_dir + config['process']['production_type'] +\
                                          name_prefix + "run" + str(run_number) + "_config.cfg"

                    #if 'temporary' in config['process']['storage']:
                    if config['process'].get('storage', None) == 'temporary':
                        temp_dir = f'/tmp/{name_prefix}run{run_number}'
                        os.makedirs(temp_dir, exist_ok=True)
                        temp_output_dir = f'{temp_dir}/output'
                        os.makedirs(temp_output_dir, exist_ok=True)
                        temp_output_file = f'{temp_output_dir}/{name_prefix}run{run_number}'
                        output_file = temp_output_file
                        temp_log_dir = f'{temp_dir}/log'                        
                        os.makedirs(temp_log_dir, exist_ok=True)
                        temp_log_file = f'{temp_log_dir}/{name_prefix}run{run_number}.log'
                        temp_config_dir = f'{temp_dir}/config'
                        os.makedirs(temp_config_dir, exist_ok=True)
                        temp_config_file = config['process']['production_type'] + name_prefix + "run" + str(run_number) + "_config.cfg"
                        temp_run_dir = f'{temp_dir}/run_directory'
                        os.makedirs(temp_run_dir, exist_ok=True)

                    
                    LOGGER.debug("job config file name %s", job_config_filename)
                    new_config = new_config.replace('@run@', str(run_number) + "0" + str(help_counter))
                    new_config = new_config.replace('@output_filename@', output_file)
                    for _ in range(1, 5):
                        seed = randint(100000, 1000000)
                        new_config = new_config.replace(f'@seed{_}@', str(seed))
                    new_config_file = open(job_config_filename, 'w+')
                    new_config_file.write(new_config)
                    new_config_file.close()
                    LOGGER.debug('NEW CONFIG FILE %s', new_config_file)
                    #if 'temporary' in config['process']['storage']:
                    if config['process'].get('storage', None) == 'temporary':
                        job_commands.append(f'mkdir {temp_dir}; mkdir {temp_output_dir}; mkdir {temp_log_dir}; mkdir {temp_config_dir};'
                                            + f'cp {job_config_filename} {temp_config_dir}; srun '
                                            + job_command(config) + f'-o {temp_output_file} < {temp_config_dir}/{temp_config_file} > {temp_log_file};'
                                            + f'cp {temp_output_file}.corsika.gz {output_dir} ; cp {temp_log_file} {log_dir} ;'
                                            + f'rm -rf {temp_dir}'
                        )
                    else:
                        job_commands.append(job_command(config) + f'-o {output_file} < {job_config_filename} > {log_file}')
            else:
                new_config = template_config
                new_config = new_config.replace('@run@', str(run_number))
                output_file = output_path + "/" + f"{config['process']['production_type']}_run{run_number}"
                new_config = new_config.replace('@output_filename@', output_file)
                LOGGER.debug("config, environment %s", config['environment'])
                for _ in range(1, 5):
                    seed = randint(100000, 1000000)
                    new_config = new_config.replace(f'@seed{_}@', str(seed))
                job_config_filename = config['environment']['submission']['run_directory'] +\
                                      f"/{config['process']['production_type']}_run{run_number}_config.cfg"
                new_config_file = open(job_config_filename, 'w+')
                new_config_file.write(new_config)
                new_config_file.close()
                log_file = f'{output_path}/log/corsika_run_{run_number}.log'
                LOGGER.debug('NEW CONFIG FILE %s', new_config_file)
                job_commands.append(job_command(config) + f" < {job_config_filename} > {log_file} ")
    elif stage == 'sim_telarray':
        list_of_parameters = []
        if 'parameter_card' in config['process']['production_type']:
            list_of_parameters = config['process']['parameter_card']['parameter_list']
        scan_parameters = []
        if 'parameter_scan' in config['process']['production_type']:
            scan_parameters = range(config['process']['parameter_scan']['initial'],
                                    config['process']['parameter_scan']['final'] + 1,  # include final value
                                    config['process']['parameter_scan']['step']
                                    )
        if config['environment']['submission']['tool'] == 'htcondor':
            output_path = ''
        else:
            output_path = config['environment'][stage]['output_path'] + '/'
        for input_file in os.listdir(config['environment'][stage]['input_path']):
            input_file_full_path = config['environment'][stage]['input_path'] + '/' + input_file
            if scan_parameters:
                for scan_parameter in scan_parameters:
                    _parameter_block = config['process']['parameter_scan']['parameter_block']
                    _parameter_name = config['process']['parameter_scan']['parameter']
                    # Update the parameter of interest on the fly
                    config['sim_telarray'][_parameter_block][_parameter_name] = scan_parameter
                    hist_file = output_path + 'hist/' + input_file[:-11] +\
                        f'_{_parameter_name}_{scan_parameter}' + '.hdata'
                    output_file = output_path + input_file[:-11] +\
                        f'_{_parameter_name}_{scan_parameter}' + '.simtel.gz'
                    log_file = output_path + 'log/' + input_file[:-11] +\
                        f'_{_parameter_name}_{scan_parameter}' + '.log'
                    if config['environment']['submission']['tool'] == 'htcondor':
                        job_commands.append(job_command(config) +
                                            f'-h {hist_file} -o {output_file} {input_file}')
                    else:
                        job_commands.append(job_command(config) +
                                            f'-h {hist_file} -o {output_file} {input_file_full_path} 2>&1 > {log_file}')
            elif list_of_parameters:
                add_command = ''
                for par in list_of_parameters:
                    LOGGER.debug("parameter %s", par)
                    value = find_parameter_value(str(input_file), str(par) + '_', '_')
                    LOGGER.debug('value %s', value)
                    # not an optimal way for now it copes only with angles
                    if str(par) == 'theta':
                        key = 'telescope_zenith_angle'
                    if str(par) == 'az':
                        key = 'telescope_phi'
                    add_command += f"-C {key}={value} "
                    add_command += "-C show=all "
                hist_file = output_path + 'hist/' + input_file[:-11] + '.hdata'
                output_file = output_path + input_file[:-11] + '.simtel.gz'
                log_file = output_path + 'log/' + input_file[:-11] + '.log'
                if config['environment']['submission']['tool'] == 'htcondor':
                    job_commands.append(job_command(config) + add_command +
                                        f'-h {hist_file} -o {output_file} {input_file}')
                else:
                    job_commands.append(job_command(config) + add_command +
                                        f'-h {hist_file} -o {output_file} {input_file_full_path} 2>&1 > {log_file}')
            else:
                hist_file = output_path + 'hist/' + input_file[:-11] + '.hdata'
                output_file = output_path + input_file[:-11] + '.simtel.gz'
                log_file = output_path + 'log/' + input_file[:-11] + '.log'
                if 'light_emission' in config['process']['production_type']:
                    job_commands.append(job_command(config) +
                                        f'-h {hist_file} -o {output_file} -i {input_file_full_path}')
                else:
                    if config['environment']['submission']['tool'] == 'htcondor':
                        job_commands.append(job_command(config) +
                                            f'-h {hist_file} -o {output_file} {input_file}')
                    else:
                        job_commands.append(job_command(config) +
                                            f'-h {hist_file} -o {output_file} {input_file_full_path} 2>&1 > {log_file}')

    elif stage == 'dl1':
        lstchain_config_file = f"{config['environment']['dl1']['temporary_local_configuration_path']}/" +\
                               f"{config['environment']['dl1']['entry_point']}"

        output_path = config['environment'][stage]['output_path']
        if config['environment']['submission']['tool'] == 'local':
            for input_file in os.listdir(config['environment'][stage]['input_path']):
                input_file_full_path = config['environment'][stage]['input_path'] + '/' + input_file
                log_file = output_path + '/log/' + input_file[:-10] + '.log'

            job_commands.append(job_command(config) +
                                f' --input-file {input_file_full_path} --output_dir {output_path}' +
                                f' --config {lstchain_config_file} 2>&1 > {log_file}')
        elif config['environment']['submission']['tool'] == 'slurm':
            n_files_per_job = config['dl1']['n_files_per_job']
            input_dir = config['environment'][stage]['input_path']
            log_level = logging.getLevelName(max([h.level for h in LOGGER.handlers]))
            job_commands.append(f"convert_r0_to_dl1 "
                                f"-i {input_dir} "
                                f"-o {output_path} "
                                f"-c {lstchain_config_file} "
                                f"-n {n_files_per_job} "
                                f"-a $SLURM_ARRAY_TASK_ID "
                                f"-l {log_level}")

        elif config['environment']['submission']['tool'] == 'sge':
            pass  # TODO Implement sge batch submission
        else:
            LOGGER.error('Unknown submission tool!')
            sys.exit(os.EX_CONFIG)
    else:
        LOGGER.error('Unknown simulation stage!')
        sys.exit(os.EX_CONFIG)

    LOGGER.debug('Created commands:\n%s', '\n'.join(job_commands))
    return job_commands


def submit_jobs(config, job_commands):
    """
    Submit jobs to either one of the supported submission engines or just run them locally

    :param dict config: Dictionary with the sim_runner configuration
    :param list(str) job_commands: List with job commands
    """
    stage = config['process']['stage']
    if stage == 'sim_telarray':
        if config['environment']['submission']['tool'] == "htcondor":
            os.makedirs(config['environment'][stage]['output_path'], exist_ok=True)
        else:
            os.makedirs(config['environment'][stage]['output_path'] + '/hist/', exist_ok=True)
            os.makedirs(config['environment'][stage]['output_path'] + '/log/', exist_ok=True)
    run_directory = f"{config['environment']['submission']['run_directory']}/"
    os.makedirs(run_directory, exist_ok=True)
    os.makedirs(run_directory + '/error/', exist_ok=True)
    os.makedirs(run_directory + '/log/', exist_ok=True)
    os.makedirs(run_directory + '/jobs/', exist_ok=True)
    workdir = os.path.realpath(run_directory)
    if stage == 'dl1':
        os.makedirs(config['environment'][stage]['output_path'] + '/log/', exist_ok=True)
    for _, cmd in enumerate(job_commands):
        if config['environment']['submission']['tool'] == "local":
            split_command = shlex.split(cmd)
            LOGGER.debug('Split command:\n%s', split_command)
            pids = []  # list of submitted job processes
            if config['process']['dry_run']:
                LOGGER.info("Dry run requested. Not submitting jobs and cleaning temporary files. Bye!")
                continue
            if stage == 'corsika':
                log_file_name = run_directory + split_command[-1]
            else:
                log_file_name = split_command[-1]
            try:
                with open(log_file_name, 'w+') as log_file:
                    if stage == 'corsika':
                        sources = config['environment']['corsika']['sources']
                        for src in sources:
                            try:
                                os.symlink(f"{config['environment']['corsika']['software_intallation_path']}/{src}",
                                           f"{run_directory}/{src}")
                            except OSError:
                                os.unlink(f"{run_directory}/{src}")
                                os.symlink(f"{config['environment']['corsika']['software_intallation_path']}/{src}",
                                           f"{run_directory}/{src}")
                        LOGGER.debug('Running %s\n with log file %s\n in directory %s',
                                     split_command, log_file, workdir)
                        pid = subprocess.Popen([cmd, ], shell=True, stdout=log_file,
                                               stderr=subprocess.STDOUT, cwd=workdir)

                    else:
                        pid = subprocess.Popen(split_command, stdout=log_file, stderr=subprocess.STDOUT)
                    pids.append(pid)
                LOGGER.info("Locally launching job %s with PID %s", _, pid.pid)
            except subprocess.SubprocessError as err:
                LOGGER.error("%s\nCaught error", err)
            exit_codes = [p.wait() for p in pids]
            LOGGER.debug("Job exit codes are\n%s", exit_codes)
        else:
            tool = config['environment']['submission']['tool']
            LOGGER.info("%s jobs will be submitted with %s", len(job_commands), tool.upper())
            job_name = f"job_{config['process']['stage']}_"\
                       f"{config['process']['production_type']}_{_+1}.job"
            job_file_path = run_directory  + '/jobs/' + job_name
            try:
                with open(config['environment']['submission']['job_template'], 'r') as template_file:
                    template = template_file.read()
            except KeyError as err:
                LOGGER.warning("Job template %s is missing, using default one", err)
                template = pkg_resources.read_text(templates, f'{stage}_{tool}.job')
            if config['environment']['submission']['tool'] == 'htcondor':
                script_file_path = f'{job_file_path[:-3]}sh'
                split_command = shlex.split(cmd)
                template = template.replace('__executable__', script_file_path)
                condor_sim_telarray_script = f'#!/bin/bash\n{split_command[0]} {" ".join(split_command[1:])}'
                with open(script_file_path, 'w') as f:
                    f.write(condor_sim_telarray_script)
                template = template.replace('__outputdir__', config['environment'][stage]['output_path'])
                template = template.replace('__input_file__',
                                            f"{config['environment'][stage]['input_path']}/{split_command[-1]}")
            else:
                template = template.replace('__command__', cmd)
            if stage == 'dl1':
                log_file_path = run_directory + '/log/' + job_name[:-4] + '-%A-%a' + '.log'
                error_file_path = run_directory + '/error/' + job_name[:-4] + '-%A-%a' + '.err'
                conda_environment = config['environment']['dl1']['conda_environment']
                template = template.replace('__condaenv__', conda_environment)
            else:
                log_file_path = run_directory + '/log/' + job_name[:-3] + 'log'
                error_file_path = run_directory + '/error/' + job_name[:-3] + 'err'
            template = template.replace('__stdoutput__', log_file_path)
            template = template.replace('__stderror__', error_file_path)
            if stage == 'corsika':
                template = template.replace('__run_directory__', run_directory)
                template = template.replace('__sw_install_path__',
                                            config['environment']['corsika']['software_intallation_path'])
                sources = config['environment']['corsika']['sources']
                for src in sources:
                    LOGGER.debug('trying to link file %s', src)
                    LOGGER.debug('from source %s', f"{config['environment']['corsika']['software_intallation_path']}")
                    LOGGER.debug('to destination %s', f"{run_directory}")
                    try:
                        os.symlink(f"{config['environment']['corsika']['software_intallation_path']}/{src}",
                                   f"{run_directory}/{src}")
                    except OSError:
                        os.unlink(f"{run_directory}/{src}")
                        os.symlink(f"{config['environment']['corsika']['software_intallation_path']}/{src}",
                                   f"{run_directory}/{src}")
                    except FileNotFoundError:
                        LOGGER.error('Corsika source file was not found: %s', src)
                if config['environment']['corsika']['version'] == '6.9':
                    template = template.replace('II-04', 'II-03')
            LOGGER.debug("Job %s content:\n%s", _, template)
            with open(job_file_path, 'w+') as new_job:
                new_job.write(template)
            if config['process']['dry_run']:
                LOGGER.info("Dry run requested. Not submitting jobs and cleaning temporary files. Bye!")
                continue
            try:
                if tool == "sge":
                    if stage == 'dl1':
                        LOGGER.warning('SGE submission for R0 to DL1 conversion is not available yet')
                        sys.exit(os.EX_CONFIG)
                    LOGGER.info("Submitting job:\n%s",
                                f"qsub {job_file_path}")
                    job_id = subprocess.check_output(['qsub', job_file_path],
                                                     stderr=subprocess.STDOUT)
                elif tool == 'htcondor':
                    if stage == 'dl1':
                        LOGGER.warning('HTCondor submission for R0 to DL1 conversion is not available yet')
                        sys.exit(os.EX_CONFIG)
                    LOGGER.info("Submitting job:\n%s",
                                f"condor_submit {job_file_path}")
                    job_id = subprocess.check_output(['condor_submit', job_file_path],
                                                     stderr=subprocess.STDOUT)
                else:
                    if stage == 'dl1':
                        input_dir = config['environment'][stage]['input_path']
                        n_files_to_analyze = len([name for name in os.listdir(input_dir)
                                                  if os.path.isfile(os.path.join(input_dir, name))])
                        n_files_per_job = config['dl1']['n_files_per_job']
                        array_n = n_files_to_analyze // n_files_per_job - 1
                        LOGGER.info("Submitting job:\n%s",
                                    f"sbatch --array=0-{array_n} {job_file_path}")
                        job_id = subprocess.check_output(['sbatch', f'--array=0-{array_n}', job_file_path],
                                                         stderr=subprocess.STDOUT)

                    else:
                        LOGGER.info("Submitting job:\n%s",
                                    f"sbatch {job_file_path}")
                        job_id = subprocess.check_output(['sbatch', job_file_path],
                                                         stderr=subprocess.STDOUT)
                LOGGER.info("Submitted job %s", job_id)
            except subprocess.SubprocessError as err:
                LOGGER.error("%s", err)
                # TODO define what to do in case of job submission failure


def run(config_file_name, verbosity):
    """
    Parse input configuration file, validate it and run simulations accordingly.
    """
    setup_logging(verbosity)
    config = {}
    try:
        config = toml.load(config_file_name)
    except (FileNotFoundError,
            toml.TomlDecodeError):
        LOGGER.error("Problem during TOML configuration reading:\n%s\n Exiting...",
                     traceback.format_exc())
        sys.exit(os.EX_CONFIG)
    LOGGER.info("Loaded configuration: \n%s", toml.dumps(config))
    LOGGER.debug("Parsed configuration: \n%s", json.dumps(config, indent=2))
    try:
        validate_config(config)
    except KeyError as err:
        LOGGER.error("Error in configuration file validation:\n%s", err)
        clean(config)
        sys.exit(os.EX_CONFIG)
    if not config['environment'].pop('use_non_tagged_configuration', False):
        get_configuration_by_tag(config)
    else:
        LOGGER.warning("Using non-tagged confguration!")
    commands = create_commands(config)
    LOGGER.info(commands)
    LOGGER.info("Submitting jobs")
    submit_jobs(config, commands)
